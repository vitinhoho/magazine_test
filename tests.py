import os
import unittest
from unittest.mock import patch
import json

from app import app, db
from app.models import PersonModel

BASEDIR = os.path.abspath(os.path.dirname(__file__))


def person():
    person = PersonModel(
        name='Victor',
        username='victor.hos',
        facebook_id=3988885827818235
    )
    db.session.add(person)
    db.session.commit()
    return person


class PersonTest(unittest.TestCase):

    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///{}'.format(
            os.path.join('{}{}'.format(BASEDIR, '/tmp'), 'test.db'))
        self.app = app.test_client()
        db.create_all()

    def tearDown(self):
        is_delete = PersonModel.query.filter().delete()

    def test_person_get(self):
        response = self.app.get('/person/')
        data = json.loads(response.data.decode('utf-8'))

        self.assertEqual(data['meta']['totalCount'], 0)
        self.assertEqual(response.status, '200 OK')

    def test_person_post_person(self):
        response = self.app.post(
            '/person/',
            data={'facebookId': 3988885827818235}
        )
        self.assertEqual(response.status, '201 CREATED')

    def test_person_post_person_conflict(self):
        # resp_first = self.app.post('/person/', data={'facebookId': 3988885827818235})
        person()
        resp_second = self.app.post('/person/', data={'facebookId': 3988885827818235})
        # self.assertEqual(resp_first.status, '201 CREATED')
        self.assertEqual(resp_second.status, '409 CONFLICT')

    def test_person_post_person_does_not_exist(self):
        response = self.app.post('/person/', data={'facebookId': '10q'})
        self.assertEqual(response.status, '404 NOT FOUND')

    def test_person_put_person_update_not_content(self):
        resp_first = self.app.post('/person/', data={'facebookId': 3988885827818235})
        resp_second = self.app.put('/person/3988885827818235/')
        self.assertEqual(resp_first.status, '201 CREATED')
        self.assertEqual(resp_second.status, '204 NO CONTENT')

    @patch('app.controllers.get_facebook_data')
    def test_person_put_person_update(self, get_facebook_data):
        get_facebook_data.return_value = {
            'name': 'Ben Green',
            'gender': None,
            'username': 'benjamin.ezra.green'
        }
        resp_first = self.app.post('/person/', data={'facebookId': 421})
        self.assertEqual(resp_first.status, '201 CREATED')

        get_facebook_data.return_value = {
            'name': 'Ben Blue',
            'gender': None,
            'username': 'benjamin.ezra.blue'
        }
        resp_second = self.app.put('/person/421/')
        self.assertEqual(resp_second.status, '200 OK')

    def test_person_put_person_does_not_exist(self):
        response = self.app.put('/person/11/')
        self.assertEqual(response.status, '404 NOT FOUND')

    def test_person_delete(self):
        resp_first = self.app.post('/person/', data={'facebookId': 3988885827818235})
        self.assertEqual(resp_first.status, '201 CREATED')

        resp_second = self.app.delete('/person/3988885827818235/')
        self.assertEqual(resp_second.status, '204 NO CONTENT')

    def test_person_delete_person_does_not_exist(self):
        resp = self.app.delete('/person/401/')
        self.assertEqual(resp.status, '404 NOT FOUND')


if __name__ == '__main__':
    unittest.main()
