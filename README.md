# Desafio LuizaLabs

Passos para rodar o projeto:

```sh
$ pip install -r requirements.txt
$ python manager.py create_db
$ python manager.py run
```
Rodar os testes:
```sh
$ python manager.py test
```

Coverage report:
```sh
Name                 Stmts   Miss  Cover   Missing
--------------------------------------------------
app/__init__.py          8      0   100%
app/controllers.py      49      3    94%   52, 64, 79
app/models.py           18      1    94%   26
app/utils.py            18      0   100%
app/views.py             4      0   100%
--------------------------------------------------
TOTAL                   97      4    96%
```