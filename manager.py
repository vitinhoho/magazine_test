import os
import unittest

from flask_script import Manager

from app import app, db


BASEDIR = os.path.abspath(os.path.dirname(__file__))
manager = Manager(app)


@manager.command
def test():
    print('[Start tests...]')
    tests = unittest.TestLoader().discover('tests')
    unittest.TextTestRunner(verbosity=2).run(tests)
    print('[Delete db]')
    os.remove(os.path.join('{}{}'.format(BASEDIR, '/tmp'), 'test.db'))


@manager.command
def create_db():
    print('[Create database]')
    db.create_all()


@manager.command
def drop_db():
    print('[Drop database]')
    db.drop_all()


@manager.command
def run():
    app.run(debug=False)


if __name__ == '__main__':
    manager.run()
