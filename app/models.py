from datetime import datetime
from app import db


class PersonModel(db.Model):

    __tablename__ = 'person'

    id = db.Column(
        db.Integer, primary_key=True
    )
    name = db.Column(
        db.String(256)
    )
    username = db.Column(
        db.String(128),
        unique=True
    )
    gender = db.Column(
        db.String(32),
        nullable=True
    )
    facebook_id = db.Column(
        db.Integer
    )
    created = db.Column(
        db.DateTime(),
        default=datetime.utcnow,
        nullable=False
    )
    updated = db.Column(
        db.DateTime(),
        default=datetime.utcnow,
        nullable=False,
        onupdate=datetime.utcnow
    )

    def __init__(self, name, username, facebook_id, gender=None):
        self.name = name
        self.username = username
        self.facebook_id = facebook_id
        self.gender = gender

    def __repr__(self):
        return '<Person {}>'.format(self.facebook_id)
