from app import api
from app.controllers import Person, PersonList


api.add_resource(PersonList, '/person/')
api.add_resource(Person, '/person/<int:facebook_id>/')
