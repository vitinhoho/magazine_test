import logging

import requests

from facebook import GraphAPI


def get_facebook_data(facebook_id, access_token):
    access_token = 'EAAUJmNw9ZB8ABAJxyuOYuSUUWRzT8E5FHxiuXipZBN2XszdFphwZA5PnR0JTOLy1YpPvGZBZCthwvqS5o9SACExwIZCORz63Udx49oA6kQl0iJXnyMLll6WakdViSrpx4VYghrNTpUocZAzWdKu6HNthnCl9kc0YGdiBORSZB6CxBroDZBZAYqUXKLmIiTH0z5ark0ZBv0c5v4QDhZByn33ZAcC9nZC7UCLoFjyH8d6ZCiCExZCFegZDZD'
    try:
        graph = GraphAPI(access_token=access_token)
        graph_objects = graph.get_object(
            str(facebook_id),
            fields='id,name'
        )
    except Exception as e:
        raise e

    return {
        'name': graph_objects.get('name', None),
        'username': get_username(facebook_id),
        'gender': graph_objects.get('gender', None)
    }


def get_username(facebook_id):
    fb_url = 'https://www.facebook.com/{}'.format(facebook_id)
    username = requests.get(fb_url).url
    # example: ['https:', '', 'www.facebook.com', 'victor.hugo.52438']
    #
    return username.split('/')[-1]


def status(status_code, detail=None):
    return {
        'status': [{
            'status_code': status_code,
            'message': detail
        }]
    }, status_code
