from flask_restful import Resource, reqparse
from app import app, db
from app.utils import get_facebook_data, status
from app.models import PersonModel


class Person(Resource):

    def put(self, facebook_id):
        person = PersonModel.query.filter(
            PersonModel.facebook_id == facebook_id).first()

        if person:

            fb_person = get_facebook_data(
                facebook_id,
                app.config['FACEBOOK_APP']['access_token']
            )

            if (
                fb_person['name'] == person.name and
                fb_person['gender'] == person.gender
            ):
                return status(204)

            else:
                person = PersonModel.query.filter(
                    PersonModel.facebook_id == facebook_id
                ).update({
                    'name': fb_person['name'],
                    'gender': fb_person['gender']
                })

            return status(200, 'facebook_id: {} created'.format(facebook_id))
        else:
            return status(404, "Person doen't exist")

    def delete(self, facebook_id):
        is_delete = PersonModel.query.filter(
            PersonModel.facebook_id == facebook_id
        ).delete()

        if is_delete:
            return status(204)
        else:
            return status(404, "Person doen't exist")


class PersonList(Resource):

    def __init__(self):
        super(PersonList, self).__init__()

        self.parser = reqparse.RequestParser()
        self.parser.add_argument(
            'limit',
            type=int,
            location='args',
            required=False
        )
        self.parser.add_argument(
            'facebookId',
            location='form'
        )

    def get(self):
        limit = self.parser.parse_args()['limit']
        if limit:
            list_persons = PersonModel.query.limit(limit).all()
        else:
            list_persons = PersonModel.query.all()

        persons = {
            'data': [],
            'meta': {
                'totalCount': PersonModel.query.count()
            }
        }

        for person in list_persons:
            persons['data'].append({
                'name': person.name,
                'username': person.username,
                'gender': person.gender,
                'facebookId': person.facebook_id,
                'created': person.created.isoformat(),
                'updated': person.updated.isoformat()
            })

        return persons, 200

    def post(self):
        facebook_id = self.parser.parse_args().get('facebookId')
        if not facebook_id:
            return status(404, "Person doen't exist")

        person_exists = PersonModel.query.filter(
            PersonModel.facebook_id == facebook_id).first()

        if person_exists:
            return status(409, 'Conflict')

        try:
            fb_person = get_facebook_data(
                facebook_id,
                app.config['FACEBOOK_APP']['access_token']
            )
            person = PersonModel(
                fb_person['name'],
                fb_person['username'],
                facebook_id, fb_person['gender']
            )
            db.session.add(person)
            db.session.commit()

            return status(201, 'facebook_id: {} created'.format(facebook_id))

        except Exception as e:
            return status(404, e.message)
